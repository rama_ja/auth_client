<?php


//use App\Http\Controllers\Api\Resturant\AuthController;

use App\Http\Controllers\Api\Resturant\AuthController;
use App\Http\Controllers\Api\Resturant\ExtraController;
use App\Http\Controllers\Api\Resturant\MealController;
use App\Http\Controllers\Api\Resturant\MealImageController;
use App\Http\Controllers\Api\Resturant\PackageController;
use App\Http\Controllers\Api\Resturant\PackageImageController;
use App\Http\Controllers\Api\Resturant\RepresentativeController;
use App\Http\Controllers\GeneralController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Client Authentication
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'client_auth'], function () {
    Route::post('/register', [App\Http\Controllers\Api\Client\AuthController::class, 'register']);
    Route::post('/login', [App\Http\Controllers\Api\Client\AuthController::class, 'login']);
    Route::get('/cities', [AuthController::class, 'getCities']);

    Route::group(['middleware' => 'auth:client_api'], function () {
        Route::post('/update_client_info', [App\Http\Controllers\Api\Client\AuthController::class, 'updateClientInfo']);
        Route::post('/update_client_password', [App\Http\Controllers\Api\Client\AuthController::class, 'updatePassword']);
        Route::post('/logout', [App\Http\Controllers\Api\Client\AuthController::class, 'logout']);
    });
});

/*
|--------------------------------------------------------------------------
| Resturant Authentication
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'resturant_auth'], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/cities', [AuthController::class, 'getCities']);

    Route::group(['middleware' => 'auth:resturant_api'], function () {
        Route::post('/add_restaurant_details', [AuthController::class, 'addRestaurantDetails']);
        Route::post('/upload_documents', [AuthController::class, 'uploadDocuments']);
        Route::post('/set_working_hours', [AuthController::class, 'setWorkingHours']);
        Route::post('/logout', [AuthController::class, 'logout']);
    });
});



## Resturant Routes
Route::group(['middleware' => 'auth:resturant_api'], function () {

    Route::group(['prefix' => 'resturant_app'], function () {

        ## Extra Routes
        Route::group(['prefix' => 'extras'], function () {
            Route::get('index', [ExtraController::class, 'index']);
            Route::post('store', [ExtraController::class, 'store']);
            Route::post('update/{id}', [ExtraController::class, 'update']);
            Route::post('destroy/{id}', [ExtraController::class, 'destroy']);
        });

        ## Representative Routes
        Route::group(['prefix' => 'Representatives'], function () {
            Route::get('index', [RepresentativeController::class, 'index']);
            Route::post('store', [RepresentativeController::class, 'store']);
            Route::get('get_cities', [RepresentativeController::class, 'getCities']);
            Route::post('update/{id}', [RepresentativeController::class, 'update']);
            Route::post('destroy/{id}', [RepresentativeController::class, 'destroy']);
        });

        ## Meals Routes
        Route::group(['prefix' => 'meals'], function () {
            Route::get('index', [MealController::class, 'index']);
            Route::post('store', [MealController::class, 'store']);
            Route::get('get_extras', [MealController::class, 'getExtras']);
            Route::post('update/{id}', [MealController::class, 'update']);
            Route::post('destroy/{id}', [MealController::class, 'destroy']);
            Route::post('add_discount', [MealController::class, 'addDiscount']);
            Route::post('change_status/{id}', [MealController::class, 'changeStatus']);
        });



        ## Meal Images Routes
        Route::group(['prefix' => 'meal_images'], function () {
            Route::get('index', [MealImageController::class, 'index']);
            Route::post('store', [MealImageController::class, 'store']);
            Route::post('destroy/{id}', [MealImageController::class, 'destroy']);
        });

        ## Packages Routes
        Route::group(['prefix' => 'packages'], function () {
            Route::get('index', [PackageController::class, 'index']);
            Route::post('store', [PackageController::class, 'store']);
            Route::get('get_days', [PackageController::class, 'getDays']);
            Route::post('update/{id}', [PackageController::class, 'update']);
            Route::post('destroy/{id}', [PackageController::class, 'destroy']);
            Route::post('change_status/{id}', [PackageController::class, 'changeStatus']);
        });

        ## Package Images Routes
        Route::group(['prefix' => 'package_images'], function () {
            Route::get('index', [PackageImageController::class, 'index']);
            Route::post('store', [PackageImageController::class, 'store']);
            Route::post('destroy/{id}', [PackageImageController::class, 'destroy']);
        });

        ## Change Language
        Route::post('change_language', [AuthController::class, 'changeLanguage']);
    });


});

## Client Routes
Route::group(['prefix' => 'client'], function () {
});

## DeliveryMan Routes
Route::group(['prefix' => 'delivery_man'], function () {
});

## General Routes
Route::group(['prefix' => 'general'], function () {

    Route::get('terms_and_conditions', [GeneralController::class, 'getTermsAndConditions']);
    Route::get('about_app', [GeneralController::class, 'getAboutApp']);
});
