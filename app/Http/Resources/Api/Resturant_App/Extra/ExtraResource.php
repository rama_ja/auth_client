<?php

namespace App\Http\Resources\Api\Resturant_App\Extra;

use Illuminate\Http\Resources\Json\JsonResource;

class ExtraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'resturant_id'         => $this->resturant_id,
            'name_ar'              => $this->translate('ar')->name,
            'name_en'              => $this->translate('en')->name,
            'price'                => $this->price,
            'image'                => $this->image,
            'description_ar'       => $this->translate('ar')->description,
            'description_en'       => $this->translate('en')->description,
        ];
    }
}
