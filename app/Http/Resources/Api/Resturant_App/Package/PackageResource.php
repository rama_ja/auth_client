<?php

namespace App\Http\Resources\Api\Resturant_App\Package;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'resturant_id'        => $this->resturant_id,
            'name_ar'             => $this->translate('ar')->name,
            'name_en'             => $this->translate('en')->name,
            'price'               => $this->price,
            'total_calories'      => $this->total_calories,
            'carbohydrate'        => $this->carbohydrate,
            'protein'             => $this->protein,
            'fats'                => $this->fats,
            'main_image'          => $this->main_image,
            'status'              => $this->status,
            'description_ar'      => $this->translate('ar')->description,
            'description_en'      => $this->translate('en')->description,
            'images'              => $this->images ?  PackageImagesResource::collection($this->images) : null,
            'details'             => $this->details ? PackageDetailsResource::collection($this->details) : null,
        ];
    }
}
