<?php

namespace App\Http\Resources\Api\Resturant_App\Package;

use App\Http\Resources\Api\Resturant_App\Resturant\DayResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'day'                 => new DayResource($this->day),
            'delivery_time'       => $this->delivery_time,
            'description_ar'      => $this->translate('ar')->description,
            'description_en'      => $this->translate('en')->description,
        ];
    }
}
