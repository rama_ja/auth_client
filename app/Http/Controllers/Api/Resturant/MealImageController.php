<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Resturant_App\Meal\MealImagesResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\MealImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class MealImageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $meal_images = MealImage::where('meal_id', $request->meal_id)->get();
        return $this->apiResponse(MealImagesResource::collection($meal_images), 'All Images for this Meal', 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'meal_id'   => 'required',
            'images'    => 'required|array|max:10',
            'images.*'  => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $final_array = [];

        if ($request->file('images')) {
            foreach ($request->file('images') as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Meals/MealImages', 400, 400);
                $new_image = MealImage::create(['meal_id' => $request->meal_id, 'image' => $image_name_DataBase]);
                array_push($final_array, $new_image);
            }
        }

        return $this->apiResponse(MealImagesResource::collection($final_array), 'The Image has been created successfully', 200);
    }

    public function destroy($id)
    {
        $meal_image = MealImage::find($id);
        (File::exists($meal_image->image)) ? File::delete($meal_image->image) : Null;
        $meal_image->delete();
        return $this->apiResponse(null, 'The meal image has been deleted successfully', 200);
    }
}
