<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Meal\StoreMealDiscountRequest;
use App\Http\Requests\Api\Resturant_App\Meal\StoreMealRequest;
use App\Http\Requests\Api\Resturant_App\Meal\UpdateMealRequest;
use App\Http\Resources\Api\Resturant_App\Extra\ExtraResource;
use App\Http\Resources\Api\Resturant_App\Meal\MealDiscountResource;
use App\Http\Resources\Api\Resturant_App\Meal\MealResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Extra;
use App\Models\Meal;
use App\Models\MealDiscount;
use App\Models\MealImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MealController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $meals = Meal::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->get();
        return $this->apiResponse(MealResource::collection($meals), 'All meals of this restaurant', 200);
    }


    public function getExtras(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $extras = Extra::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->get();
        return $this->apiResponse(ExtraResource::collection($extras), 'The required data to create or update a meal', 200);
    }

    public function store(StoreMealRequest $request)
    {
        $resturant = $request->user('resturant_api');
        $meal = Meal::create([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'total_calories'  => $request->total_calories,
            'carbohydrate'    => $request->carbohydrate,
            'protein'         => $request->protein,
            'fats'            => $request->fats,
            'main_image'      => $this->saveImage($request->file('main_image'), 'Images/Meals/MainImages', 400, 400),
            'status'          => $request->status,
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Meals/MealImages', 400, 400);
                MealImage::create(['meal_id' => $meal->id, 'image' => $image_name_DataBase]);
            }
        }

        if ($request->has_discount == '1') {
            MealDiscount::create([
                'meal_id'             => $meal->id,
                'has_discount'        => $request->has_discount,
                'discount_type'       => $request->discount_type,
                'discount_value'      => $request->discount_value,
                'discount_start_date' => $request->discount_start_date,
                'discount_end_date'   => $request->discount_end_date,
            ]);
        }

        $meal->extras()->attach($request->extras);

        return $this->apiResponse(new MealResource($meal), 'The meal has been created successfully', 200);
    }

    public function update(UpdateMealRequest $request, $id)
    {
        $resturant = $request->user('resturant_api');
        $meal = Meal::find($id);

        $meal->update([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'total_calories'  => $request->total_calories,
            'carbohydrate'    => $request->carbohydrate,
            'protein'         => $request->protein,
            'fats'            => $request->fats,
            'main_image'      =>  $this->updateImage($meal, $request, 'main_image', 'Images/Meals/MainImages', 400, 400),
            'status'          => $request->status,
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        $this->updateDiscount($meal, $request);

        $meal->extras()->sync($request->extras);

        return $this->apiResponse(new MealResource($meal), 'The meal has been updated successfully', 200);
    }

    public function destroy($id)
    {
        $meal = Meal::find($id);
        (File::exists($meal->image)) ? File::delete($meal->image) : Null;
        foreach ($meal->images()->pluck('image') as $Image) {
            (File::exists($Image)) ? File::delete($Image) : Null;
        }
        $meal->delete();
        return $this->apiResponse(null, 'The meal has been deleted successfully', 200);
    }

    public function addDiscount(StoreMealDiscountRequest $request)
    {
        $meal_discount = MealDiscount::create([
            'meal_id'             => $request->meal_id,
            'has_discount'        => '1',
            'discount_type'       => $request->discount_type,
            'discount_value'      => $request->discount_value,
            'discount_start_date' => $request->discount_start_date,
            'discount_end_date'   => $request->discount_end_date,
        ]);

        return $this->apiResponse(new MealDiscountResource($meal_discount), 'A discount has been added to this meal successfully', 200);
    }

    public function updateDiscount($meal, $request)
    {
        if (MealDiscount::where('meal_id', $meal->id)->doesntExist() && $request->has_discount == '1') {
            MealDiscount::create([
                'meal_id'             => $meal->id,
                'has_discount'        => $request->has_discount,
                'discount_type'       => $request->discount_type,
                'discount_value'      => $request->discount_value,
                'discount_start_date' => $request->discount_start_date,
                'discount_end_date'   => $request->discount_end_date,
            ]);
        } //
        elseif (MealDiscount::where('meal_id', $meal->id)->exists() && $request->has_discount == '1') {
            MealDiscount::where('meal_id', $meal->id)->update([
                'meal_id'             => $meal->id,
                'has_discount'        => $request->has_discount,
                'discount_type'       => $request->discount_type,
                'discount_value'      => $request->discount_value,
                'discount_start_date' => $request->discount_start_date,
                'discount_end_date'   => $request->discount_end_date,
            ]);
        } //
        elseif (MealDiscount::where('meal_id', $meal->id)->exists() && $request->has_discount == '0') {
            MealDiscount::where('meal_id', $meal->id)->delete();
        }
    }

    public function changeStatus(Request $request, $id)
    {
        Meal::find($id)->update(['status' => $request->status]);
        return $this->apiResponse(null, 'Meal status changed successfully', 200);
    }
}
