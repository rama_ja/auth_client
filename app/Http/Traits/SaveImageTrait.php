<?php

namespace App\Http\Traits;

use App\Models\AdvertisementImage;
use App\Models\DriverVehicleImage;
use App\Models\EventImage;
use App\Models\ResturantImage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

trait SaveImageTrait
{
    public function saveImage($photo, $folder, $width, $height)
    {
        // Saving The Image
        $imageExtension = $photo->getClientOriginalExtension();
        $image_name = time() . '.' . $imageExtension;
        $image_name_DataBase = $folder . '/' . time() . '.' . $imageExtension;
        $path = $folder . '/' . $image_name;
        Image::make($photo)->resize(
            $width,
            $height,
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            }
        )->save($path);
        return $image_name_DataBase;
    }

    public function saveFile($photo, $folder)
    {
        // Saving The Image
        $imageExtension = $photo->getClientOriginalExtension();
        $image_name_DataBase = $folder . '/' . time() . '.' . $imageExtension;
        $photo->move(public_path($folder), $image_name_DataBase);
        return $image_name_DataBase;
    }

    public function updateImage($object, $request, $image_name, $folder_name, $width, $hight)
    {
        if ($request->hasFile($image_name)) {
            $photo_destination =  $object->$image_name;
            if (File::exists($photo_destination)) {
                File::delete($photo_destination);
            }
            $image = $this->saveImage($request->file($image_name), $folder_name, $width, $hight);
            return $image;
        } else
            return $object->$image_name;
    }

    public function saveImages($Image, $folder, $width, $height)
    {
        // Store Image In Public Folder
        $imageExtension = $Image->getClientOriginalExtension();
        $image_name =  md5(rand(1000, 100000)) .  '.' . $imageExtension;
        $image_name_DataBase = $folder . '/' . $image_name;
        $path = $folder . '/' . $image_name;
        Image::make($Image)->resize(
            $width,
            $height,
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            }
        )->save($path);
        return $image_name_DataBase;
    }
}
