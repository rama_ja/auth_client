<?php

namespace App\Http\Requests\Api\Resturant_App\Extra;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExtraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'                         => 'required|string',
            'name_en'                         => 'required|string',
            'price'                           => 'required|numeric|gt:0',
            'description_ar'                  => 'required|string',
            'description_en'                  => 'required|string',
            'image'                           => 'image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
