<?php

namespace Database\Seeders;

use App\Models\Day;
use Illuminate\Database\Seeder;

class DaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Days = [
            [
                'ar' => ['name' => 'السبت'],
                'en' => ['name' => 'Saturday']
            ],
            [
                'ar' => ['name' => 'الأحد'],
                'en' => ['name' => 'Sunday']
            ],
            [
                'ar' => ['name' => 'الاثنين'],
                'en' => ['name' => 'Monday']
            ],
            [
                'ar' => ['name' => 'الثلاثاء'],
                'en' => ['name' => 'Tuesday']
            ],
            [
                'ar' => ['name' => 'الأربعاء'],
                'en' => ['name' => 'Wednesday']
            ],
            [
                'ar' => ['name' => 'الخميس'],
                'en' => ['name' => 'Thursday']
            ],
            [
                'ar' => ['name' => 'الجمعة'],
                'en' => ['name' => 'Friday']
            ],
        ];

        foreach ($Days as $Day) {
            Day::create($Day);
        }
    }
}
